<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DashBoardController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [LoginController::class,'LoginView'])->name('login.view');
Route::post('/login', [LoginController::class,'Login'])->name('login');
Route::get('/logout', [LoginController::class,'Logout'])->name('logout');
Route::view('/forgot', 'web.auth.forgot')->name('forgot');


Route::prefix('admin')->as('admin.')->middleware(['CheckAuth'])->group(function () {

    Route::get('/change-pass',[UserController::class,'PasswordUpdateView'])->name('pass.view');
    Route::post('/update-pass',[UserController::class,'PasswordUpdate'])->name('update.pass');

    Route::middleware(['CheckRole:10'])->group(function () {
        Route::get('/', [DashBoardController::class,'index'])->name('dash');
    });
});

