

@switch(Session::get('role'))
    @case(10)
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.dash') }}">
            <i class="icon-grid menu-icon"></i>
            <span class="menu-title">Dashboard</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#sites" aria-expanded="false" aria-controls="sites">
            <i class="icon-grid-2 menu-icon"></i>
            <span class="menu-title">Users</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="sites">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="">Add new</a></li>
                <li class="nav-item"> <a class="nav-link" href="">List all</a></li>
            </ul>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">
            <i class="icon-paper menu-icon"></i>
            <span class="menu-title">Documentation</span>
        </a>
    </li>
        @break

    @default

@endswitch




