<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function PasswordUpdateView()
    {
        return view('web.auth.password');
    }

    public function PasswordUpdate(Request $request)
    {
        $this->validate($request,[
            'password' => 'required|confirmed'
        ]);
        User::where('email',Session::get('email'))->update([
            'password' => Hash::make($request->password)
        ]);
        return redirect()->back()->withSuccess('Password Updated Successfully');
    }

}
