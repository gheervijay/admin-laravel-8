<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;

class LoginController extends Controller
{
        public function LoginView(){
            return view('web.auth.login');
        }

        public function Login(Request $request)
        {
            $this->validate($request,[
                'email' => 'required|email|exists:users,email',
                'password' => 'required'
            ]);
             $User = User::where(['email'=> $request->email])->first();
            if(Hash::check($request->password, $User->password)){
                switch ($User->role) {
                    case User::ADMIN:
                        Session::put(['email' => $User->email,'role' => $User->role]);
                        return redirect()->route('admin.dash')->withSuccess('Login Successfully');
                    break;
                    default:

                        return redirect()->route('login.view')->withInputs()->withErrors('Invalid Request');
                    break;
                }
            }else{
                return redirect()->route('login.view')->withErrors('Invalid Password');
            }
        }

        public function Logout()
        {
            Session::flush();
            return redirect()->route('login.view')->withSuccess('Logout Successfully');

        }
}
